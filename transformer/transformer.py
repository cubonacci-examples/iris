import pandas as pd


class Transformer():
    def __init__(self, secrets):
        self.secrets = secrets  # IE: this could open a database connection

    def transform_input(self, inputs):
        features = pd.Series({"Sepal_Length": 1, "Sepal_Width": 2, "Petal_Length": 3, "Petal_Width": 4})
        return features
